package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;
 
public class MinMaxTest {

  @Test
    public void testScenario1(){
        int maxNumber= new MinMax().findMax(4,3);
        assertEquals("Max Number -1", 4, maxNumber);

    }
   @Test
    public void testScenario2() throws Exception {

        int maxNumber= new MinMax().findMax(3,4);
        assertEquals("Max Number -2", 4, maxNumber);

    }
    @Test
    public void testCompareScenario3() throws Exception {

        String returnedStringIfNotEmpty= new MinMax().bar("manmohit");
        assertEquals("returnedStringIfNotEmpty", "manmohit", returnedStringIfNotEmpty);

        String returnedStringIfEmpty= new MinMax().bar("");
        assertEquals("returnedStringIfEmpty", "", returnedStringIfEmpty);

	String returnedStringIfNull = new MinMax().bar(null);
	assertEquals("returnedStringIfNull", null, returnedStringIfNull);
    } 
}
